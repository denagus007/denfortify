<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Denagus\Denfortify\Denfortify;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('n')//two_factor_secret
                    ->after('m')//password
                    ->nullable();

            $table->text('o')//two_factor_recovery_codes
                    ->after('n')//two_factor_secret
                    ->nullable();

            if (Denfortify::confirmsTwoFactorAuthentication()) {
                $table->timestamp('p')//two_factor_confirmed_at
                        ->after('o')//two_factor_recovery_codes
                        ->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(array_merge([
                'n',//two_factor_secret
                'o',//two_factor_recovery_codes
            ], Denfortify::confirmsTwoFactorAuthentication() ? [
                'p',//two_factor_confirmed_at
            ] : []));
        });
    }
};
