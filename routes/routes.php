<?php

use Illuminate\Support\Facades\Route;
use Denagus\Denfortify\Features;
use Denagus\Denfortify\Http\Controllers\AuthenticatedSessionController;
use Denagus\Denfortify\Http\Controllers\ConfirmablePasswordController;
use Denagus\Denfortify\Http\Controllers\ConfirmedPasswordStatusController;
use Denagus\Denfortify\Http\Controllers\ConfirmedTwoFactorAuthenticationController;
use Denagus\Denfortify\Http\Controllers\EmailVerificationNotificationController;
use Denagus\Denfortify\Http\Controllers\EmailVerificationPromptController;
use Denagus\Denfortify\Http\Controllers\NewPasswordController;
use Denagus\Denfortify\Http\Controllers\PasswordController;
use Denagus\Denfortify\Http\Controllers\PasswordResetLinkController;
use Denagus\Denfortify\Http\Controllers\ProfileInformationController;
use Denagus\Denfortify\Http\Controllers\RecoveryCodeController;
use Denagus\Denfortify\Http\Controllers\RegisteredUserController;
use Denagus\Denfortify\Http\Controllers\TwoFactorAuthenticatedSessionController;
use Denagus\Denfortify\Http\Controllers\TwoFactorAuthenticationController;
use Denagus\Denfortify\Http\Controllers\TwoFactorQrCodeController;
use Denagus\Denfortify\Http\Controllers\VerifyEmailController;

Route::group(['middleware' => config('denfortify.middleware', ['web'])], function () {
    $enableViews = config('denfortify.views', true);

    // Authentication...
    if ($enableViews) {
        Route::get('/login', [AuthenticatedSessionController::class, 'create'])
            ->middleware(['guest:'.config('denfortify.guard')])
            ->name('login');
    }

    $limiter = config('denfortify.limiters.login');
    $twoFactorLimiter = config('denfortify.limiters.two-factor');
    $verificationLimiter = config('denfortify.limiters.verification', '6,1');

    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware(array_filter([
            'guest:'.config('denfortify.guard'),
            $limiter ? 'throttle:'.$limiter : null,
        ]));

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->name('logout');

    // Password Reset...
    if (Features::enabled(Features::resetPasswords())) {
        if ($enableViews) {
            Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
                ->middleware(['guest:'.config('denfortify.guard')])
                ->name('password.request');

            Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
                ->middleware(['guest:'.config('denfortify.guard')])
                ->name('password.reset');
        }

        Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
            ->middleware(['guest:'.config('denfortify.guard')])
            ->name('password.email');

        Route::post('/reset-password', [NewPasswordController::class, 'store'])
            ->middleware(['guest:'.config('denfortify.guard')])
            ->name('password.update');
    }

    // Registration...
    if (Features::enabled(Features::registration())) {
        if ($enableViews) {
            Route::get('/register', [RegisteredUserController::class, 'create'])
                ->middleware(['guest:'.config('denfortify.guard')])
                ->name('register');
        }

        Route::post('/register', [RegisteredUserController::class, 'store'])
            ->middleware(['guest:'.config('denfortify.guard')]);
    }

    // Email Verification...
    if (Features::enabled(Features::emailVerification())) {
        if ($enableViews) {
            Route::get('/email/verify', [EmailVerificationPromptController::class, '__invoke'])
                ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')])
                ->name('verification.notice');
        }

        Route::get('/email/verify/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
            ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard'), 'signed', 'throttle:'.$verificationLimiter])
            ->name('verification.verify');

        Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
            ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard'), 'throttle:'.$verificationLimiter])
            ->name('verification.send');
    }

    // Profile Information...
    if (Features::enabled(Features::updateProfileInformation())) {
        Route::put('/user/profile-information', [ProfileInformationController::class, 'update'])
            ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')])
            ->name('user-profile-information.update');
    }

    // Passwords...
    if (Features::enabled(Features::updatePasswords())) {
        Route::put('/user/password', [PasswordController::class, 'update'])
            ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')])
            ->name('user-password.update');
    }

    // Password Confirmation...
    if ($enableViews) {
        Route::get('/user/confirm-password', [ConfirmablePasswordController::class, 'show'])
            ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')]);
    }

    Route::get('/user/confirmed-password-status', [ConfirmedPasswordStatusController::class, 'show'])
        ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')])
        ->name('password.confirmation');

    Route::post('/user/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware([config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')])
        ->name('password.confirm');

    // Two Factor Authentication...
    if (Features::enabled(Features::twoFactorAuthentication())) {
        if ($enableViews) {
            Route::get('/two-factor-challenge', [TwoFactorAuthenticatedSessionController::class, 'create'])
                ->middleware(['guest:'.config('denfortify.guard')])
                ->name('two-factor.login');
        }

        Route::post('/two-factor-challenge', [TwoFactorAuthenticatedSessionController::class, 'store'])
            ->middleware(array_filter([
                'guest:'.config('denfortify.guard'),
                $twoFactorLimiter ? 'throttle:'.$twoFactorLimiter : null,
            ]));

        $twoFactorMiddleware = Features::optionEnabled(Features::twoFactorAuthentication(), 'confirmPassword')
            ? [config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard'), 'password.confirm']
            : [config('denfortify.auth_middleware', 'auth').':'.config('denfortify.guard')];

        Route::post('/user/two-factor-authentication', [TwoFactorAuthenticationController::class, 'store'])
            ->middleware($twoFactorMiddleware)
            ->name('two-factor.enable');

        Route::post('/user/confirmed-two-factor-authentication', [ConfirmedTwoFactorAuthenticationController::class, 'store'])
            ->middleware($twoFactorMiddleware)
            ->name('two-factor.confirm');

        Route::delete('/user/two-factor-authentication', [TwoFactorAuthenticationController::class, 'destroy'])
            ->middleware($twoFactorMiddleware)
            ->name('two-factor.disable');

        Route::get('/user/two-factor-qr-code', [TwoFactorQrCodeController::class, 'show'])
            ->middleware($twoFactorMiddleware)
            ->name('two-factor.qr-code');

        Route::get('/user/two-factor-recovery-codes', [RecoveryCodeController::class, 'index'])
            ->middleware($twoFactorMiddleware)
            ->name('two-factor.recovery-codes');

        Route::post('/user/two-factor-recovery-codes', [RecoveryCodeController::class, 'store'])
            ->middleware($twoFactorMiddleware);
    }
});
