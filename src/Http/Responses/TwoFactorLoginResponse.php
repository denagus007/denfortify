<?php

namespace Denagus\Denfortify\Http\Responses;

use Illuminate\Http\JsonResponse;
use Denagus\Denfortify\Contracts\TwoFactorLoginResponse as TwoFactorLoginResponseContract;
use Denagus\Denfortify\Denfortify;

class TwoFactorLoginResponse implements TwoFactorLoginResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return $request->wantsJson()
                    ? new JsonResponse('', 204)
                    : redirect()->intended(Denfortify::redirects('login'));
    }
}
