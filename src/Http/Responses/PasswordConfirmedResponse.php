<?php

namespace Denagus\Denfortify\Http\Responses;

use Illuminate\Http\JsonResponse;
use Denagus\Denfortify\Contracts\PasswordConfirmedResponse as PasswordConfirmedResponseContract;
use Denagus\Denfortify\Denfortify;

class PasswordConfirmedResponse implements PasswordConfirmedResponseContract
{
    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return $request->wantsJson()
                    ? new JsonResponse('', 201)
                    : redirect()->intended(Denfortify::redirects('password-confirmation'));
    }
}
