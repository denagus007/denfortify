<?php

namespace Denagus\Denfortify\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Denagus\Denfortify\Contracts\VerifyEmailViewResponse;
use Denagus\Denfortify\Denfortify;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Denagus\Denfortify\Contracts\VerifyEmailViewResponse
     */
    public function __invoke(Request $request)
    {
        return $request->user()->hasVerifiedEmail()
                    ? redirect()->intended(Denfortify::redirects('email-verification'))
                    : app(VerifyEmailViewResponse::class);
    }
}
