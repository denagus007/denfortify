<?php

namespace Denagus\Denfortify\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Denagus\Denfortify\Contracts\PasswordUpdateResponse;
use Denagus\Denfortify\Contracts\UpdatesUserPasswords;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Denagus\Denfortify\Contracts\UpdatesUserPasswords  $updater
     * @return \Denagus\Denfortify\Contracts\PasswordUpdateResponse
     */
    public function update(Request $request, UpdatesUserPasswords $updater)
    {
        $updater->update($request->user(), $request->all());

        return app(PasswordUpdateResponse::class);
    }
}
