<?php

namespace Denagus\Denfortify\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface VerifyEmailViewResponse extends Responsable
{
    //
}
