<?php

namespace Denagus\Denfortify\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface FailedTwoFactorLoginResponse extends Responsable
{
    //
}
