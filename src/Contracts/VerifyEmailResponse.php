<?php

namespace Denagus\Denfortify\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface VerifyEmailResponse extends Responsable
{
    //
}
