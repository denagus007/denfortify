<?php

namespace Denagus\Denfortify\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface SuccessfulPasswordResetLinkRequestResponse extends Responsable
{
    //
}
