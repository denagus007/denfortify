<?php

namespace Denagus\Denfortify\Contracts;

use Illuminate\Contracts\Support\Responsable;

interface LoginViewResponse extends Responsable
{
    //
}
