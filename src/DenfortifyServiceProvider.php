<?php

namespace Denagus\Denfortify;

use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Contracts\Cache\Repository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Denagus\Denfortify\Contracts\FailedPasswordConfirmationResponse as FailedPasswordConfirmationResponseContract;
use Denagus\Denfortify\Contracts\FailedPasswordResetLinkRequestResponse as FailedPasswordResetLinkRequestResponseContract;
use Denagus\Denfortify\Contracts\FailedPasswordResetResponse as FailedPasswordResetResponseContract;
use Denagus\Denfortify\Contracts\FailedTwoFactorLoginResponse as FailedTwoFactorLoginResponseContract;
use Denagus\Denfortify\Contracts\LockoutResponse as LockoutResponseContract;
use Denagus\Denfortify\Contracts\LoginResponse as LoginResponseContract;
use Denagus\Denfortify\Contracts\LogoutResponse as LogoutResponseContract;
use Denagus\Denfortify\Contracts\PasswordConfirmedResponse as PasswordConfirmedResponseContract;
use Denagus\Denfortify\Contracts\PasswordResetResponse as PasswordResetResponseContract;
use Denagus\Denfortify\Contracts\PasswordUpdateResponse as PasswordUpdateResponseContract;
use Denagus\Denfortify\Contracts\RegisterResponse as RegisterResponseContract;
use Denagus\Denfortify\Contracts\SuccessfulPasswordResetLinkRequestResponse as SuccessfulPasswordResetLinkRequestResponseContract;
use Denagus\Denfortify\Contracts\TwoFactorAuthenticationProvider as TwoFactorAuthenticationProviderContract;
use Denagus\Denfortify\Contracts\TwoFactorLoginResponse as TwoFactorLoginResponseContract;
use Denagus\Denfortify\Contracts\VerifyEmailResponse as VerifyEmailResponseContract;
use Denagus\Denfortify\Http\Responses\FailedPasswordConfirmationResponse;
use Denagus\Denfortify\Http\Responses\FailedPasswordResetLinkRequestResponse;
use Denagus\Denfortify\Http\Responses\FailedPasswordResetResponse;
use Denagus\Denfortify\Http\Responses\FailedTwoFactorLoginResponse;
use Denagus\Denfortify\Http\Responses\LockoutResponse;
use Denagus\Denfortify\Http\Responses\LoginResponse;
use Denagus\Denfortify\Http\Responses\LogoutResponse;
use Denagus\Denfortify\Http\Responses\PasswordConfirmedResponse;
use Denagus\Denfortify\Http\Responses\PasswordResetResponse;
use Denagus\Denfortify\Http\Responses\PasswordUpdateResponse;
use Denagus\Denfortify\Http\Responses\RegisterResponse;
use Denagus\Denfortify\Http\Responses\SuccessfulPasswordResetLinkRequestResponse;
use Denagus\Denfortify\Http\Responses\TwoFactorLoginResponse;
use Denagus\Denfortify\Http\Responses\VerifyEmailResponse;
use PragmaRX\Google2FA\Google2FA;

class DenfortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/denfortify.php', 'fortify');

        $this->registerResponseBindings();

        $this->app->singleton(TwoFactorAuthenticationProviderContract::class, function ($app) {
            return new TwoFactorAuthenticationProvider(
                $app->make(Google2FA::class),
                $app->make(Repository::class)
            );
        });

        $this->app->bind(StatefulGuard::class, function () {
            return Auth::guard(config('denfortify.guard', null));
        });
    }

    /**
     * Register the response bindings.
     *
     * @return void
     */
    protected function registerResponseBindings()
    {
        $this->app->singleton(FailedPasswordConfirmationResponseContract::class, FailedPasswordConfirmationResponse::class);
        $this->app->singleton(FailedPasswordResetLinkRequestResponseContract::class, FailedPasswordResetLinkRequestResponse::class);
        $this->app->singleton(FailedPasswordResetResponseContract::class, FailedPasswordResetResponse::class);
        $this->app->singleton(FailedTwoFactorLoginResponseContract::class, FailedTwoFactorLoginResponse::class);
        $this->app->singleton(LockoutResponseContract::class, LockoutResponse::class);
        $this->app->singleton(LoginResponseContract::class, LoginResponse::class);
        $this->app->singleton(TwoFactorLoginResponseContract::class, TwoFactorLoginResponse::class);
        $this->app->singleton(LogoutResponseContract::class, LogoutResponse::class);
        $this->app->singleton(PasswordConfirmedResponseContract::class, PasswordConfirmedResponse::class);
        $this->app->singleton(PasswordResetResponseContract::class, PasswordResetResponse::class);
        $this->app->singleton(PasswordUpdateResponseContract::class, PasswordUpdateResponse::class);
        $this->app->singleton(RegisterResponseContract::class, RegisterResponse::class);
        $this->app->singleton(SuccessfulPasswordResetLinkRequestResponseContract::class, SuccessfulPasswordResetLinkRequestResponse::class);
        $this->app->singleton(VerifyEmailResponseContract::class, VerifyEmailResponse::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->configurePublishing();
        $this->configureRoutes();
    }

    /**
     * Configure the publishable resources offered by the package.
     *
     * @return void
     */
    protected function configurePublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../stubs/denfortify.php' => config_path('denfortify.php'),
            ], 'fortify-config');

            $this->publishes([
                __DIR__.'/../stubs/CreateNewUser.php' => app_path('Actions/Denfortify/CreateNewUser.php'),
                __DIR__.'/../stubs/DenfortifyServiceProvider.php' => app_path('Providers/DenfortifyServiceProvider.php'),
                __DIR__.'/../stubs/PasswordValidationRules.php' => app_path('Actions/Denfortify/PasswordValidationRules.php'),
                __DIR__.'/../stubs/ResetUserPassword.php' => app_path('Actions/Denfortify/ResetUserPassword.php'),
                __DIR__.'/../stubs/UpdateUserProfileInformation.php' => app_path('Actions/Denfortify/UpdateUserProfileInformation.php'),
                __DIR__.'/../stubs/UpdateUserPassword.php' => app_path('Actions/Denfortify/UpdateUserPassword.php'),
            ], 'fortify-support');

            $this->publishes([
                __DIR__.'/../database/migrations' => database_path('migrations'),
            ], 'fortify-migrations');
        }
    }

    /**
     * Configure the routes offered by the application.
     *
     * @return void
     */
    protected function configureRoutes()
    {
        if (Denfortify::$registersRoutes) {
            Route::group([
                'namespace' => 'Denagus\Denfortify\Http\Controllers',
                'domain' => config('denfortify.domain', null),
                'prefix' => config('denfortify.prefix'),
            ], function () {
                $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');
            });
        }
    }
}
