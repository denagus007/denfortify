<?php

namespace Denagus\Denfortify\Actions;

use Denagus\Denfortify\Events\TwoFactorAuthenticationDisabled;
use Denagus\Denfortify\Denfortify;

class DisableTwoFactorAuthentication
{
    /**
     * Disable two factor authentication for the user.
     *
     * @param  mixed  $user
     * @return void
     */
    public function __invoke($user)
    {
        $user->forceFill([
            'n' => null,//two_factor_secret
            'o' => null,//two_factor_recovery_codes
        ] + (Denfortify::confirmsTwoFactorAuthentication() ? [
            'p' => null,//two_factor_confirmed_at
        ] : []))->save();

        TwoFactorAuthenticationDisabled::dispatch($user);
    }
}
