<?php

namespace Denagus\Denfortify\Actions;

use Illuminate\Support\Collection;
use Denagus\Denfortify\Contracts\TwoFactorAuthenticationProvider;
use Denagus\Denfortify\Events\TwoFactorAuthenticationEnabled;
use Denagus\Denfortify\RecoveryCode;

class EnableTwoFactorAuthentication
{
    /**
     * The two factor authentication provider.
     *
     * @var \Denagus\Denfortify\Contracts\TwoFactorAuthenticationProvider
     */
    protected $provider;

    /**
     * Create a new action instance.
     *
     * @param  \Denagus\Denfortify\Contracts\TwoFactorAuthenticationProvider  $provider
     * @return void
     */
    public function __construct(TwoFactorAuthenticationProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * Enable two factor authentication for the user.
     *
     * @param  mixed  $user
     * @return void
     */
    public function __invoke($user)
    {
        $user->forceFill([
            'n' => encrypt($this->provider->generateSecretKey()),//
            'o' => encrypt(json_encode(Collection::times(8, function () {//two_factor_recovery_codes
                return RecoveryCode::generate();
            })->all())),
        ])->save();

        TwoFactorAuthenticationEnabled::dispatch($user);
    }
}
