<?php

namespace Denagus\Denfortify\Actions;

use Illuminate\Support\Collection;
use Denagus\Denfortify\Events\RecoveryCodesGenerated;
use Denagus\Denfortify\RecoveryCode;

class GenerateNewRecoveryCodes
{
    /**
     * Generate new recovery codes for the user.
     *
     * @param  mixed  $user
     * @return void
     */
    public function __invoke($user)
    {
        $user->forceFill([
            'o' => encrypt(json_encode(Collection::times(8, function () {//two_factor_recovery_codes
                return RecoveryCode::generate();
            })->all())),
        ])->save();

        RecoveryCodesGenerated::dispatch($user);
    }
}
