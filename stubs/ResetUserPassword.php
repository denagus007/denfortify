<?php

namespace App\Actions\Denfortify;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Denagus\Denfortify\Contracts\ResetsUserPasswords;

class ResetUserPassword implements ResetsUserPasswords
{
    use PasswordValidationRules;

    /**
     * Validate and reset the user's forgotten password.
     *
     * @param  mixed  $user
     * @param  array  $input
     * @return void
     */
    public function reset($user, array $input)
    {
        Validator::make($input, [
            'password' => $this->passwordRules(),
        ])->validate();

        $user->forceFill([
            'k' => Hash::make($input['password']),
        ])->save();
    }
}
