<?php

namespace App\Providers;

use App\Actions\Denfortify\CreateNewUser;
use App\Actions\Denfortify\ResetUserPassword;
use App\Actions\Denfortify\UpdateUserPassword;
use App\Actions\Denfortify\UpdateUserProfileInformation;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\ServiceProvider;
use Denagus\Denfortify\Denfortify;

class DenfortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Denfortify::createUsersUsing(CreateNewUser::class);
        Denfortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Denfortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Denfortify::resetUserPasswordsUsing(ResetUserPassword::class);

        RateLimiter::for('login', function (Request $request) {
            $email = (string) $request->email;

            return Limit::perMinute(5)->by($email.$request->ip());
        });

        RateLimiter::for('two-factor', function (Request $request) {
            return Limit::perMinute(5)->by($request->session()->get('login.id'));
        });
    }
}
